package com.tingco.codechallenge.health;

import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

public class HealthControllerTest {

    private MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new HealthController()).build();

    @Test
    public void shouldReturnOkWhenHealthy() throws Exception {
        mockMvc.perform(get("/health"))
                .andExpect(content().string("OK"));
    }

}
