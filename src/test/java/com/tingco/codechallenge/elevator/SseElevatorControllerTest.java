package com.tingco.codechallenge.elevator;

import com.google.common.eventbus.EventBus;
import com.tingco.codechallenge.api.Elevator;
import com.tingco.codechallenge.event.ElevatorEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class SseElevatorControllerTest {

    private static final ElevatorEvent DUMMY_EVENT = new ElevatorEvent(0,0, Elevator.Direction.UP);

    @Autowired
    private WebTestClient webClient;

    @Autowired
    private EventBus eventBus;

    @Test
    public void shouldPublishEventsFromEventBusAsServerSentEvents() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<?> future = executorService.submit(this::publishMessages);

        List<ElevatorEvent> events = webClient.get()
                .uri("/events")
                .accept(MediaType.TEXT_EVENT_STREAM)
                .exchange()
                .expectStatus()
                .isOk()
                .returnResult(ElevatorEvent.class)
                .getResponseBody()
                .take(3)
                .collectList()
                .block();

        future.cancel(true);

        assertThat(events).containsExactly(DUMMY_EVENT, DUMMY_EVENT, DUMMY_EVENT);
        executorService.shutdownNow();
    }

    private void publishMessages() {
        try {
            int retry = 20;
            while (--retry > 0) {
                TimeUnit.MILLISECONDS.sleep(100);
                eventBus.post(DUMMY_EVENT);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}