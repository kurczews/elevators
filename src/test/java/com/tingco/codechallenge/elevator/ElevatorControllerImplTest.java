package com.tingco.codechallenge.elevator;

import com.google.common.eventbus.EventBus;
import com.tingco.codechallenge.ShaftProperties;
import com.tingco.codechallenge.api.Elevator;
import com.tingco.codechallenge.delay.DeferredTask;
import com.tingco.codechallenge.strategy.FirstFromCollection;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

public class ElevatorControllerImplTest {

    private static final int FLOORS_NUMBER = 6;
    private static final int ELEVATORS_NUMBER = 3;

    private ElevatorControllerImpl elevatorController;
    private EventBus eventBus = new EventBus();

    @Before
    public void setUp() {
        ElevatorFactory elevatorFactory = new ElevatorFactory(mock(EventBus.class), new DeferredTask(Duration.ZERO));
        elevatorController = spy(new ElevatorControllerImpl(new ShaftProperties(ELEVATORS_NUMBER, FLOORS_NUMBER),
                elevatorFactory, new FirstFromCollection(), eventBus, Runnable::run));
    }

    @Test
    public void whenElevatorRequestedThenMoveElevatorAndMarkBusy() {
        // when
        Elevator elevator = elevatorController.requestElevator(2);

        // then
        assertThat(elevator.currentFloor()).isEqualTo(2);
        assertThat(elevator.isBusy()).isEqualTo(true);
    }

    @Test
    public void requestedElevatorCanBeMovedByReference() {
        // given
        Elevator elevator = elevatorController.requestElevator(2);

        // when
        elevator.moveElevator(3);

        // then
        assertThat(elevator.currentFloor()).isEqualTo(3);
        assertThat(elevator.isBusy()).isEqualTo(true);
    }

    @Test
    public void whenElevatorReleasedThenMarkElevatorIdle() {
        // given
        Elevator elevator = elevatorController.requestElevator(2);
        int id = elevator.getId();

        // when
        elevatorController.releaseElevator(elevator);

        // then
        Elevator storedElevator = retrieveStoredElevator(id);

        assertThat(storedElevator.currentFloor()).isEqualTo(2);
        assertThat(storedElevator.isBusy()).isEqualTo(false);
    }

    @Test
    public void shouldReturnConfiguredNumberOfFloors() {
        assertThat(elevatorController.getFloorsNumber()).isEqualTo(FLOORS_NUMBER);
    }

    private Elevator retrieveStoredElevator(int id) {
        return elevatorController
                .getElevators()
                .stream()
                .filter(it -> it.getId() == id)
                .findFirst()
                .orElseThrow(NoSuchElementException::new);
    }
}