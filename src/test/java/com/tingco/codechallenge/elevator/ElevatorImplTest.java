package com.tingco.codechallenge.elevator;

import com.google.common.eventbus.EventBus;
import com.tingco.codechallenge.api.Elevator;
import com.tingco.codechallenge.delay.DeferredTask;
import com.tingco.codechallenge.event.ElevatorEvent;
import com.tingco.codechallenge.event.ElevatorEventListener;
import org.junit.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ElevatorImplTest {

    private EventBus eventBus = new EventBus();
    private ElevatorFactory elevatorFactory = new ElevatorFactory(eventBus, new DeferredTask(Duration.ZERO));

    @Test
    public void afterCreationElevatorIsIdleOnZeroFloor() {
        // given
        Elevator elevator = elevatorFactory.newElevator(1);

        // then
        assertThat(elevator.getId()).isEqualTo(1);
        assertThat(elevator.currentFloor()).isEqualTo(0);
        assertThat(elevator.getAddressedFloor()).isEqualTo(0);
        assertThat(elevator.isBusy()).isEqualTo(false);
        assertThat(elevator.getDirection()).isEqualTo(Elevator.Direction.NONE);
    }

    @Test
    public void whenMovingElevatorToUpperFloorGenerateMovedUpEvents() {
        // given
        Elevator elevator = elevatorFactory.newElevator(1);

        List<ElevatorEvent> events = new ArrayList<>();
        ElevatorEventListener listener = events::add;
        eventBus.register(listener);

        // when
        elevator.moveElevator(3);

        // then
        assertThat(events).containsExactly(
                new ElevatorEvent(1, 1, Elevator.Direction.UP),
                new ElevatorEvent(1, 2, Elevator.Direction.UP),
                new ElevatorEvent(1, 3, Elevator.Direction.NONE)
        );
    }

    @Test
    public void whenMovingElevatorToLowerFloorGenerateMovedDownEvents() {
        // given
        Elevator elevator = elevatorFactory.newElevator(1);
        elevator.moveElevator(3);

        List<ElevatorEvent> events = new ArrayList<>();
        ElevatorEventListener listener = events::add;
        eventBus.register(listener);

        // when
        elevator.moveElevator(0);

        // then
        assertThat(events).containsExactly(
                new ElevatorEvent(1, 2, Elevator.Direction.DOWN),
                new ElevatorEvent(1, 1, Elevator.Direction.DOWN),
                new ElevatorEvent(1, 0, Elevator.Direction.NONE)
        );
    }
}