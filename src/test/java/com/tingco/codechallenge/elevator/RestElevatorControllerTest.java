package com.tingco.codechallenge.elevator;

import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static java.net.HttpURLConnection.HTTP_OK;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RestElevatorControllerTest {

    private ElevatorControllerImpl serviceMock = mock(ElevatorControllerImpl.class);
    private MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new RestElevatorController(serviceMock)).build();

    @Test
    public void shouldReturnListOfElevators() throws Exception {
        mockMvc.perform(get("/elevators"))
                .andExpect(status().is(HTTP_OK));

        verify(serviceMock).getElevators();
        verifyNoMoreInteractions(serviceMock);
    }

    @Test
    public void shouldRequestElevator() throws Exception {
        mockMvc.perform(post("/elevators/request/{floor}", 2))
                .andExpect(status().is(HTTP_OK));

        verify(serviceMock).requestElevator(2);
        verifyNoMoreInteractions(serviceMock);
    }

    @Test
    public void shouldReturnNumberOfFloors() throws Exception {
        when(serviceMock.getFloorsNumber()).thenReturn(6);

        mockMvc.perform(get("/elevators/floors"))
                .andExpect(status().is(HTTP_OK))
                .andExpect(content().string("6"));
    }
}