package com.tingco.codechallenge.elevator;

import com.google.common.eventbus.EventBus;
import com.tingco.codechallenge.api.Elevator;
import com.tingco.codechallenge.delay.BlockingDeferredTask;
import com.tingco.codechallenge.event.ElevatorEvent;
import com.tingco.codechallenge.event.ElevatorEventListener;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.assertj.core.api.Assertions.assertThat;

public class ThreadElevatorImplTest {

    private ExecutorService executor = Executors.newSingleThreadExecutor();
    private BlockingDeferredTask moveDelayer = new BlockingDeferredTask(Duration.ZERO, Duration.ofSeconds(2));
    private ArrayList<ElevatorEvent> events = new ArrayList<>();
    private ElevatorFactory elevatorFactory;

    @Before
    public void setUp() {
        EventBus eventBus = new EventBus();
        eventBus.register((ElevatorEventListener) events::add);

        elevatorFactory = new ElevatorFactory(eventBus, moveDelayer);
    }

    @After
    public void tearDown() {
        executor.shutdown();
    }

    @Test
    public void verifyEachProgressState() throws Exception {
        // given
        Elevator elevator = elevatorFactory.newElevator(1);

        // when
        Future<?> future = executor.submit(() -> elevator.moveElevator(3));

        // then
        moveDelayer.poke();
        assertThat(events).containsExactly(new ElevatorEvent(1, 1, Elevator.Direction.UP));
        assertThat(elevator.currentFloor()).isEqualTo(1);
        assertThat(elevator.getDirection()).isEqualTo(Elevator.Direction.UP);
        assertThat(elevator.isBusy()).isEqualTo(true);

        moveDelayer.poke();
        assertThat(events).containsExactly(
                new ElevatorEvent(1, 1, Elevator.Direction.UP),
                new ElevatorEvent(1, 2, Elevator.Direction.UP)
        );
        assertThat(elevator.currentFloor()).isEqualTo(2);
        assertThat(elevator.getDirection()).isEqualTo(Elevator.Direction.UP);
        assertThat(elevator.isBusy()).isEqualTo(true);

        moveDelayer.poke();
        assertThat(events).containsExactly(
                new ElevatorEvent(1, 1, Elevator.Direction.UP),
                new ElevatorEvent(1, 2, Elevator.Direction.UP),
                new ElevatorEvent(1, 3, Elevator.Direction.NONE)
        );
        assertThat(elevator.currentFloor()).isEqualTo(3);
        assertThat(elevator.getDirection()).isEqualTo(Elevator.Direction.NONE);
        assertThat(elevator.isBusy()).isEqualTo(true);

        future.get();
    }
}