package com.tingco.codechallenge.strategy;

import com.tingco.codechallenge.api.Elevator;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RoundRobinTest {

    @Test
    public void shouldReturnInstancesInRoundRobinFashion() {
        // given
        RoundRobin objectRoundRobin = new RoundRobin();
        Elevator first = mock(Elevator.class);
        Elevator second = mock(Elevator.class);
        Elevator third = mock(Elevator.class);
        List<Elevator> items = Arrays.asList(first, second, third);

        // when & then
        assertThat(objectRoundRobin.choose(items)).isEqualTo(first);
        assertThat(objectRoundRobin.choose(items)).isEqualTo(second);
        assertThat(objectRoundRobin.choose(items)).isEqualTo(third);
        assertThat(objectRoundRobin.choose(items)).isEqualTo(first);
    }

    @Test
    public void shouldSkipBusyElevatorsWhenTraversingCollection() {
        // given
        RoundRobin objectRoundRobin = new RoundRobin();

        Elevator alwaysBusy = mock(Elevator.class);
        when(alwaysBusy.isBusy()).thenReturn(true);

        Elevator first = mock(Elevator.class);
        Elevator second = mock(Elevator.class);
        Elevator third = mock(Elevator.class);
        List<Elevator> items = Arrays.asList(first, second, alwaysBusy, third);

        // when & then
        assertThat(objectRoundRobin.choose(items)).isEqualTo(first);
        assertThat(objectRoundRobin.choose(items)).isEqualTo(second);
        assertThat(objectRoundRobin.choose(items)).isEqualTo(third);
        assertThat(objectRoundRobin.choose(items)).isEqualTo(first);
    }
}