package com.tingco.codechallenge;

public class ShaftProperties {

    private final int elevatorsNumber;
    private final int floorsNumber;

    public ShaftProperties(int elevatorsNumber, int floorsNumber) {
        this.elevatorsNumber = elevatorsNumber;
        this.floorsNumber = floorsNumber;
    }

    public int getElevatorsNumber() {
        return elevatorsNumber;
    }

    public int getFloorsNumber() {
        return floorsNumber;
    }
}
