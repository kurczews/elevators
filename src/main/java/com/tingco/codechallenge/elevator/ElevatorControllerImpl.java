package com.tingco.codechallenge.elevator;

import com.google.common.eventbus.EventBus;
import com.tingco.codechallenge.ShaftProperties;
import com.tingco.codechallenge.api.Elevator;
import com.tingco.codechallenge.api.ElevatorController;
import com.tingco.codechallenge.event.ElevatorEvent;
import com.tingco.codechallenge.event.ElevatorEventListener;
import com.tingco.codechallenge.strategy.ChoosingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.stream.IntStream;

import static com.tingco.codechallenge.api.Elevator.Direction;

@Service
class ElevatorControllerImpl implements ElevatorController, ElevatorEventListener {

    private final ShaftProperties shaftProperties;
    private final ElevatorFactory elevatorFactory;
    private final ChoosingStrategy choosingStrategy;
    private final Executor executor;
    private final ConcurrentHashMap<Integer, Elevator> elevators = new ConcurrentHashMap<>();

    @Autowired
    ElevatorControllerImpl(ShaftProperties shaftProperties, ElevatorFactory elevatorFactory,
                           ChoosingStrategy choosingStrategy, EventBus eventBus,
                           @Qualifier("taskExecutor") Executor executor) {
        this.shaftProperties = shaftProperties;
        this.elevatorFactory = elevatorFactory;
        this.choosingStrategy = choosingStrategy;
        this.executor = executor;

        eventBus.register(this);
        IntStream
                .range(0, shaftProperties.getElevatorsNumber())
                .mapToObj(elevatorFactory::newElevator)
                .forEach(elevator -> elevators.put(elevator.getId(), elevator));
    }

    @Override
    public Elevator requestElevator(int toFloor) {
        Elevator elevator = choosingStrategy.choose(elevators.values());
        executor.execute(() -> elevator.moveElevator(toFloor));
        return elevator;
    }

    @Override
    public List<Elevator> getElevators() {
        return new ArrayList<>(elevators.values());
    }

    @Override
    public void handleEvent(ElevatorEvent event) {
        if (event.getDirection() == Direction.NONE) {
            releaseElevator(elevators.get(event.getId()));
        }
    }

    /**
     * Release given elevator, further use of passed reference is not recommended and behaviour
     * in such case is undefined.
     *
     * @param elevator elevator to release
     */
    public void releaseElevator(Elevator elevator) {
        elevators.computeIfPresent(elevator.getId(), (id, old) -> elevatorFactory.markIdle(old));
    }

    int getFloorsNumber() {
        return shaftProperties.getFloorsNumber();
    }
}