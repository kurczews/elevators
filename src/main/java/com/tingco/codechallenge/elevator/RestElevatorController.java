package com.tingco.codechallenge.elevator;

import com.tingco.codechallenge.api.Elevator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
class RestElevatorController {

    private final ElevatorControllerImpl elevatorController;

    @Autowired
    RestElevatorController(ElevatorControllerImpl elevatorController) {
        this.elevatorController = elevatorController;
    }

    @GetMapping("/elevators")
    public List<Elevator> getElevators() {
        return elevatorController.getElevators();
    }

    @GetMapping("/elevators/floors")
    public int getFloorNumber() {
        return elevatorController.getFloorsNumber();
    }

    @PostMapping("/elevators/request/{floor}")
    public Elevator requestElevator(@PathVariable int floor) {
        return elevatorController.requestElevator(floor);
    }

}