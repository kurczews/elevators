package com.tingco.codechallenge.elevator;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.google.common.eventbus.EventBus;
import com.tingco.codechallenge.api.Elevator;
import com.tingco.codechallenge.delay.DeferredTask;
import com.tingco.codechallenge.event.ElevatorEvent;

class ElevatorImpl implements Elevator {

    private final int id;
    private final EventBus eventBus;
    private final DeferredTask delayer;

    private int currentFloor;
    private int addressedFloor;
    private Direction direction;
    private boolean busy;

    ElevatorImpl(int id, EventBus eventBus, DeferredTask delayer) {
        this(id, eventBus, delayer, 0, Direction.NONE, 0, false);
    }

    ElevatorImpl(int id, EventBus eventBus, DeferredTask delayer, int currentFloor,
                 Direction direction, int addressedFloor, boolean busy) {
        this.id = id;
        this.eventBus = eventBus;
        this.delayer = delayer;
        this.currentFloor = currentFloor;
        this.direction = direction;
        this.addressedFloor = addressedFloor;
        this.busy = busy;
    }

    /**
     * Move elevator to target floor in blocking manner
     *
     * @param toFloor target floor
     */
    @Override
    public synchronized void moveElevator(int toFloor) {
        if (toFloor == currentFloor) {
            return;
        }

        busy = true;
        addressedFloor = toFloor;
        direction = toFloor > currentFloor ? Direction.UP : Direction.DOWN;

        do {
            delayer.run(() -> {
                if (direction == Direction.UP) {
                    ++currentFloor;
                } else {
                    --currentFloor;
                }
                eventBus.post(ElevatorEvent.from(this));
            });
        } while (currentFloor != addressedFloor);
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    @JsonGetter("currentFloor")
    public int currentFloor() {
        return currentFloor;
    }

    @Override
    public int getAddressedFloor() {
        return addressedFloor;
    }

    @Override
    public Direction getDirection() {
        int direction = currentFloor - addressedFloor;
        if (direction > 0) {
            return Direction.DOWN;
        } else if (direction < 0) {
            return Direction.UP;
        } else {
            return Direction.NONE;
        }
    }

    @Override
    public boolean isBusy() {
        return busy;
    }
}
