package com.tingco.codechallenge.elevator;

import com.google.common.eventbus.EventBus;
import com.tingco.codechallenge.api.Elevator;
import com.tingco.codechallenge.delay.DeferredTask;

public class ElevatorFactory {

    private final EventBus eventBus;
    private final DeferredTask delayer;

    public ElevatorFactory(EventBus eventBus, DeferredTask delayer) {
        this.eventBus = eventBus;
        this.delayer = delayer;
    }

    Elevator newElevator(int id) {
        return new ElevatorImpl(id, eventBus, delayer);
    }

    Elevator markIdle(Elevator elevator) {
        return new ElevatorImpl(elevator.getId(), eventBus, delayer,
                elevator.currentFloor(), elevator.getDirection(), elevator.getAddressedFloor(), false);
    }
}
