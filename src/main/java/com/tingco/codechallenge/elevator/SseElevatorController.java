package com.tingco.codechallenge.elevator;

import com.google.common.eventbus.EventBus;
import com.tingco.codechallenge.event.ElevatorEvent;
import com.tingco.codechallenge.event.ElevatorEventListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.synchronizedList;
import static org.springframework.http.MediaType.TEXT_EVENT_STREAM_VALUE;

@Controller
@CrossOrigin(origins = "http://localhost:3000")
class SseElevatorController implements ElevatorEventListener {

    private List<FluxSink<ElevatorEvent>> subscribers = synchronizedList(new ArrayList<>());

    @Autowired
    public SseElevatorController(EventBus eventBus) {
        eventBus.register(this);
    }

    @GetMapping(value = "/events", produces = TEXT_EVENT_STREAM_VALUE)
    public Flux<ElevatorEvent> streamEvents() {
        return Flux.create(sink -> {
            sink.onDispose(() -> subscribers.remove(sink));
            subscribers.add(sink);
        });
    }

    @Override
    public void handleEvent(ElevatorEvent event) {
        subscribers.forEach(subscriber -> subscriber.next(event));
    }
}