package com.tingco.codechallenge.elevator;

import com.tingco.codechallenge.api.Elevator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Random;

import static org.springframework.http.MediaType.TEXT_EVENT_STREAM_VALUE;

@RestController
class LoadGenerator {

    private final Random random = new Random();

    @Autowired
    private ElevatorControllerImpl elevatorController;

    @GetMapping(value = "/simulate", produces = TEXT_EVENT_STREAM_VALUE)
    public Flux<Elevator> startSimulation() {
        return Flux.interval(Duration.ofSeconds(2))
                .map(it -> elevatorController.requestElevator(chooseRandomFloor()));
    }

    private int chooseRandomFloor() {
        return random.nextInt(elevatorController.getFloorsNumber());
    }
}