package com.tingco.codechallenge.delay;

import java.time.Duration;

public class DeferredTask {
    private final Duration delay;

    /**
     * Delays execution of passed tasks by given delay
     *
     * @param delay duration of delay
     */
    public DeferredTask(Duration delay) {
        this.delay = delay;
    }

    /**
     * Runs given task with configured delay in current thread
     *
     * @param task task to be executed
     */
    public void run(Runnable task) {
        try {
            Thread.sleep(delay.toMillis(), delay.getNano());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        task.run();
    }
}
