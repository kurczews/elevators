package com.tingco.codechallenge.delay;

import java.time.Duration;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;

public class BlockingDeferredTask extends DeferredTask {

    /**
     * This code uses BlockingQueue to avoid hassle with spurious wake ups,
     * missed signals etc. when using {@link Object#wait()} or {@link Condition#wait()}
     */
    private final BlockingQueue<Boolean> waitUntilPoke = new ArrayBlockingQueue<>(1);
    private final BlockingQueue<Boolean> waitUntilTaskDone = new ArrayBlockingQueue<>(1);
    private final Duration timeout;

    /**
     * Delays execution of passed tasks by given delay in blocking manner
     *
     * @param delay   duration of delay
     * @param timeout delay before exception is thrown
     */
    public BlockingDeferredTask(Duration delay, Duration timeout) {
        super(delay);
        this.timeout = timeout;
    }

    /**
     * Blocks until {@link BlockingDeferredTask#poke()} is called. Then calls {@link DeferredTask#run(Runnable)}.
     *
     * @param task task to be executed
     */
    public void run(Runnable task) {
        try {
            if (waitUntilPoke.poll(timeout.toMillis(), TimeUnit.MILLISECONDS) == null) {
                throw new TimeoutException("No poke signal received before timeout");
            }
        } catch (InterruptedException | TimeoutException e) {
            throw new RuntimeException(e);
        }
        super.run(task);
        waitUntilTaskDone.offer(true);
    }

    /**
     * Blocks until {@link BlockingDeferredTask#run(Runnable)} finish or timeout expires.
     */
    public void poke() throws InterruptedException, TimeoutException {
        if (!waitUntilPoke.offer(true, timeout.toMillis(), TimeUnit.MILLISECONDS)) {
            throw new TimeoutException("Previous poke was not consumed in given timeout");
        }
        if (waitUntilTaskDone.poll(timeout.toMillis(), TimeUnit.MILLISECONDS) == null) {
            throw new TimeoutException("Task not finished in given timeout");
        }
    }
}
