package com.tingco.codechallenge;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import com.tingco.codechallenge.delay.DeferredTask;
import com.tingco.codechallenge.elevator.ElevatorFactory;
import com.tingco.codechallenge.strategy.ChoosingStrategy;
import com.tingco.codechallenge.strategy.RoundRobin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.task.TaskExecutorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.time.Duration;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Configuration
public class ElevatorConfiguration {

    @Value("${com.tingco.elevator.elevatorsNumber}")
    private int elevatorsNumber;

    @Value("${com.tingco.elevator.floorsNumber}")
    private int floorsNumber;

    @Bean(destroyMethod = "shutdown")
    public Executor taskExecutor() {
        return Executors.newScheduledThreadPool(elevatorsNumber);
    }

    @Bean
    public ThreadPoolTaskExecutor applicationTaskExecutor(TaskExecutorBuilder builder) {
        return builder.build();
    }

    @Bean
    public EventBus eventBus() {
        return new AsyncEventBus(Executors.newCachedThreadPool());
    }

    @Bean
    public DeferredTask elevatorMoveDelayer() {
        return new DeferredTask(Duration.ofSeconds(2));
    }

    @Bean
    public ElevatorFactory elevatorFactory(EventBus eventBus, DeferredTask moveDelay) {
        return new ElevatorFactory(eventBus, moveDelay);
    }

    @Bean
    public ShaftProperties shaftProperties() {
        return new ShaftProperties(elevatorsNumber, floorsNumber);
    }

    @Bean
    public ChoosingStrategy choosingStrategy() {
        return new RoundRobin();
    }
}
