package com.tingco.codechallenge.event;

import com.google.common.eventbus.Subscribe;

@FunctionalInterface
public interface ElevatorEventListener {

    @Subscribe
    void handleEvent(ElevatorEvent event);
}
