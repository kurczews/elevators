package com.tingco.codechallenge.event;

import com.tingco.codechallenge.api.Elevator;

import java.util.Objects;

import static com.tingco.codechallenge.api.Elevator.Direction;

public class ElevatorEvent {

    private final int id;
    private final Direction direction;
    private final int currentFloor;

    public static ElevatorEvent from(Elevator elevator) {
        return new ElevatorEvent(elevator.getId(), elevator.currentFloor(), elevator.getDirection());
    }

    public ElevatorEvent(int id, int currentFloor, Direction direction) {
        this.id = id;
        this.direction = direction;
        this.currentFloor = currentFloor;
    }

    public int getId() {
        return id;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ElevatorEvent that = (ElevatorEvent) o;
        return id == that.id &&
                currentFloor == that.currentFloor &&
                direction == that.direction;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, direction, currentFloor);
    }

    @Override
    public String toString() {
        return "ElevatorEvent{" +
                "id=" + id +
                ", direction=" + direction +
                ", currentFloor=" + currentFloor +
                '}';
    }
}
