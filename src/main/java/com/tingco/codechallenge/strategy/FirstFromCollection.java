package com.tingco.codechallenge.strategy;

import com.tingco.codechallenge.api.Elevator;

import java.util.Collection;
import java.util.NoSuchElementException;

public class FirstFromCollection implements ChoosingStrategy {
    @Override
    public Elevator choose(Collection<Elevator> entries) {
        return entries.stream().findFirst().orElseThrow(NoSuchElementException::new);
    }
}
