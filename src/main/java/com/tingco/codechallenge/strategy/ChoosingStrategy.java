package com.tingco.codechallenge.strategy;

import com.tingco.codechallenge.api.Elevator;

import java.util.Collection;

@FunctionalInterface
public interface ChoosingStrategy {
    Elevator choose(Collection<Elevator> entries);
}
