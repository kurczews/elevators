package com.tingco.codechallenge.strategy;

import com.tingco.codechallenge.api.Elevator;

import java.util.Collection;

public class RoundRobin implements ChoosingStrategy {

    private int i = -1;

    @Override
    public Elevator choose(Collection<Elevator> entries) {
        Elevator elevator;
        do {
            elevator = (Elevator) entries.toArray()[++i % entries.size()];
        } while(elevator.isBusy());
        return elevator;
    }
}
