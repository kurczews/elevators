# Elevators challenge

Checkout GUI for this project [here](https://bitbucket.org/kurczews/elevators-ui/src/master/).

Requirements are [here](README.original.md).

## Build and run

```
mvn clean package
java -jar target/elevator-1.0-SNAPSHOT.jar
```

## Bad things I am aware

* `Impl`, I don't like that stuff but with given interfaces I had no room for anything better
* one of accessors in `Elevator` interface is named badly and is fixed via annotation
* there is async exception thrown by Tomcat randomly (I tried to fix it but I failed miserably), this is known issue btw.
* strategy of choosing elevator is the simplest one
* there is no integration test, I had no idea what could be exactly tested
* `Elevator` interface doesn't expose setters while being obviously mutable (via `moveElevator` method), in result design of `releaseElevator` is somewhat hacky